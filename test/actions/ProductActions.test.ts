import * as actions from '../../src/actions/ProductActions';
import {
    RESET_PRODUCTS,
    SET_LOADING_PRODUCTS,
    RESET_BRANDS,
    GET_PRODUCTS,
    GET_PRODUCTS_SUCCESS, GET_PRODUCTS_FAIL
} from '../../src/actions/types';
import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock'
import Tag from "../../src/model/Tag";

let data = {
    "products": [
        {
            "brand_name": "adidas Originals",
            "family_articles": [
                {
                    "brand_name": null,
                    "family_articles": null,
                    "media": [
                        {
                            "packet_shot": true,
                            "path": "AD/11/2B/0N/QQ/11/AD112B0NQ-Q11@12.jpg",
                            "role": "FAMILY",
                        },
                    ],
                    "name": "X_PLR - Trainers - core black/trace grey metallic",
                    "price": {
                        "has_different_prices": false,
                        "has_discount_on_selected_sizes_only": false,
                        "original": "£69.99",
                        "promotional": "£52.49",
                    },
                    "sizes": [
                        "3.5",
                        "4",
                        "4.5",
                        "5",
                        "5.5",
                        "6",
                        "6.5",
                        "7",
                        "7.5",
                        "8",
                        "8.5",
                        "9",
                        "9.5",
                        "10",
                        "10.5",
                        "11",
                        "11.5",
                        "12",
                        "13",
                        "13.5",
                    ],
                    "sku": "AD112B0NQ-Q11",
                    "url_key": "adidas-originals-trainers-core-blacktrace-grey-metallic-ad112b0nq-q11",
                },
            ],
            "media": [
                {
                    "packet_shot": true,
                    "path": "AD/11/2B/0N/QQ/11/AD112B0NQ-Q11@12.jpg",
                    "role": "DEFAULT",
                },
            ],
            "name": "X_PLR - Trainers - core black/trace grey metallic",
            "price": {
                "has_different_prices": false,
                "has_discount_on_selected_sizes_only": false,
                "original": "£69.99",
                "promotional": "£52.49",
            },
            "sizes": [
                "3.5",
                "4",
                "4.5",
                "5",
                "5.5",
                "6",
                "6.5",
                "7",
                "7.5",
                "8",
                "8.5",
                "9",
                "9.5",
                "10",
                "10.5",
                "11",
                "11.5",
                "12",
                "13",
                "13.5",
            ],
            "sku": "AD112B0NQ-Q11",
            "url_key": "adidas-originals-trainers-core-blacktrace-grey-metallic-ad112b0nq-q11",
        }
    ],
    "tags": [
        {
            "enabled": true,
            "importance": 0.7071,
            "text": "Sneakers",
            "type": "label",
        },
        {
            "enabled": true,
            "importance": 0.7095,
            "text": "Adidas",
            "type": "label",
        }
    ],
};

let brands: Array<string> = ["adidas Originals"];
let currentLogosAndLabels: Array<Tag> = [new Tag('Sneakers', true, 'label', 0.7095)];
let logosAndLabels: Array<Tag> = [new Tag('Sneakers', true, 'label', 0.7095)];

describe('actions', () => {
    it('should create an action to reset the products list', () => {
        const expectedAction = {
            type: RESET_PRODUCTS
        };
        expect(actions.resetProducts()).toEqual(expectedAction);
    });

    it('should create an action the set the loading value', () => {
        const value = true;
        const expectedAction = {
            type: SET_LOADING_PRODUCTS,
            payload: value
        };
        expect(actions.setLoadingProducts(value)).toEqual(expectedAction);
    });

    it('should create an action to reset the brands list', () => {
        const expectedAction = {
            type: RESET_BRANDS
        };
        expect(actions.resetBrands()).toEqual(expectedAction);
    });

    it('should return brands', () => {
        expect(actions.getBrands(data)).toEqual(brands);
    });

    it('should return new logosAndLabels', () => {
        expect(actions.getUsedLogosAndLabels(data, currentLogosAndLabels)).toEqual(logosAndLabels);
    });

    it('should handle the start of fetching products', () => {
        const expectedAction = {type: GET_PRODUCTS};
        expect(actions.getProductsStarted()).toEqual(expectedAction);
    });

    it('should handle the success of fetching products', () => {
        const products = data.products;
        const expectedAction = {
            type: GET_PRODUCTS_SUCCESS,
            payload: {products: products, brands: brands}
        };
        expect(actions.getProductsSuccess(data, [])).toEqual(expectedAction);
    });

    it('should handle the failure of fetching products', () => {
        const error: string = 'error';
        const expectedAction = {
            type: GET_PRODUCTS_FAIL,
            payload: error
        };
        expect(actions.getProductsFail(error)).toEqual(expectedAction);
    });
});



import React, { Component } from 'react';
import {
    Button,
    Text
} from 'native-base';
import { connect } from 'react-redux';
import { View, StyleSheet, TouchableOpacity, FlatList, Animated } from 'react-native';
import { getAdjustedFontSize, SNAP } from '../config/Constants';
import { FontAwesome } from '@expo/vector-icons';
import { addTag } from '../actions/PhotoActions';
import { resetProducts } from '../actions/ProductActions';

class FilterScreen extends Component<any, any> {
    static navigationOptions = {
        header: null
    };

    state = {fadeAnim: new Animated.Value(0)};

    componentDidMount() {
        Animated.timing(this.state.fadeAnim, {
            toValue: 1, duration: 500
        }).start();
    }

    render() {
        const {containerStyle, headerTextStyle, closeButtonStyle} = styles;
        const {genders, navigation, brands} = this.props;
        const headerText = navigation.getParam('filter', '');
        const optionsArray: Array<string> = headerText === 'gender' ? genders : brands;
        return (
            <View style={containerStyle}>
                <Text style={headerTextStyle}>{headerText.toString().toUpperCase()}:</Text>
                {this.renderOptions(optionsArray)}
                <View style={{alignSelf: 'center', flex: 0.2}}>
                    <TouchableOpacity onPress={() => navigation.navigate(SNAP)} style={closeButtonStyle}>
                        <FontAwesome name="remove" size={40} color="white"/>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }

    onFilterClicked(text: string) {
        const {navigation, addTag, data, resetProducts} = this.props;
        addTag(text, data);
        resetProducts();
        navigation.navigate(SNAP);
    }

    renderOptions(optionsArray: Array<string>) {
        const {buttonTextStyle, buttonStyle} = styles;
        return (
            <FlatList
                style={{flex: 0.7}}
                data={optionsArray}
                renderItem={({item}) => (
                    <Button style={buttonStyle} rounded={true} large={true} onPress={() => this.onFilterClicked(item)}>
                        <Text style={buttonTextStyle}>{item.toUpperCase()}</Text>
                    </Button>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        );
    }
}

const styles = StyleSheet.create({
   containerStyle: {
       backgroundColor: '#a3cbef',
       alignItems: 'center',
       flex: 1
   },
    headerTextStyle: {
        fontSize: getAdjustedFontSize(46),
        color: '#273c75',
        fontWeight: '700',
        flex: 0.1,
        marginTop: 30
    },
    buttonStyle: {
        backgroundColor: '#273c75',
        alignSelf: 'center',
        justifyContent: 'center',
        width: 200,
        marginBottom: 15,
        marginTop: 15
    },
    buttonTextStyle: {
        color: '#fff',
        fontWeight: '800'
    },
    closeButtonStyle: {
        height: 80,
        width: 80,
        borderRadius: 50,
        backgroundColor: '#273c75',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

const mapStateToProps = ({photo, product}: {photo: any, product: any}) => {
  const {data} = photo;
  const {products, brands, genders} = product;
  return {genders, brands, data, products};
};

export default connect(mapStateToProps, {addTag, resetProducts})(FilterScreen);
import {
    CLEAR_IMAGES,
    GET_PRODUCTS,
    GET_PRODUCTS_FAIL,
    GET_PRODUCTS_SUCCESS, RESET_BRANDS, RESET_PRODUCTS,
    SET_LOADING_PRODUCTS
} from '../actions/types';

const INITIAL_STATE = {
    products: [],
    loadingProducts: false,
    productError: '',
    genders: ['Men', 'Women'],
    brands: []
};

export default (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return {...state, products: [], loadingProducts: true, productError: ''};
        case GET_PRODUCTS_SUCCESS:
            return {...state, products: [].concat(action.payload.products), loadingProducts: false, productError: '', brands: [].concat(action.payload.brands)};
        case GET_PRODUCTS_FAIL:
            return {...state, products: [], productError: action.payload, loadingProducts: false, brands: []};
        case SET_LOADING_PRODUCTS:
            return {...state, loadingProducts: action.payload};
        case RESET_PRODUCTS:
            return {...state, products: []};
        case RESET_BRANDS:
            return {...state, brands: []};
        default:
            return state;
    }
};
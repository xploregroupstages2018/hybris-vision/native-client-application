import * as actions from '../../src/actions/CameraActions';
import { RESET_SETTINGS, SET_FIRST_PHOTO, SET_FLASH, SET_PERMISSION, SET_TYPE, SET_ZOOM } from '../../src/actions/types';

describe('actions', () => {
    it('should create an action to reset settings of camera', () => {
        const expectedAction = {
            type: RESET_SETTINGS,
        };
        expect(actions.resetSettings()).toEqual(expectedAction);
    }) ;

    it('should create an action to set the photo to use in SnapScreen', () => {
        const photo: string = "";
        const expectedAction = {
            type: SET_FIRST_PHOTO,
            payload: photo
        };
        expect(actions.setFirstPhoto(photo)).toEqual(expectedAction);
    }) ;

    it('should create an action to set the value of camera permission', () => {
        const permission: boolean = true;
        const expectedAction = {
            type: SET_PERMISSION,
            payload: permission
        };
        expect(actions.setPermission(permission)).toEqual(expectedAction);
    }) ;

    it('should create an action to switch between front/back camera', () => {
        const type: string = 'back';
        const expectedAction = {
            type: SET_TYPE,
            payload: type
        };
        expect(actions.setType(type)).toEqual(expectedAction);
    }) ;

    it('should create an action to set the value of zoom', () => {
        const zoom: number = 0.5;
        const expectedAction = {
            type: SET_ZOOM,
            payload: zoom
        };
        expect(actions.setZoom(zoom)).toEqual(expectedAction);
    }) ;

    it('should create an action to set the value of the flash', () => {
        const flash: number = 0;
        const expectedAction = {
            type: SET_FLASH,
            payload: flash
        };
        expect(actions.setFlash(flash)).toEqual(expectedAction);
    }) ;

});
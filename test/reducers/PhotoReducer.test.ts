import PhotoReducer from '../../src/reducers/PhotoReducer';
import { CLEAR_SNAP, GET_TAGS, GET_TAGS_FAIL, GET_TAGS_SUCCESS, SET_SNAP, SET_URI, SET_CONNECTED, REMOVE_ITEM,
        ADD_TAG, RESET_EXTRA_TAGS, SET_LOGOS_AND_LABELS} from '../../src/actions/types';
import Tag from "../../src/model/Tag";

describe('Photo reducer', () => {
    it('should return the initial state', () => {
        expect(PhotoReducer(undefined, {}))
            .toEqual({
                uri: '',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: false,
                extraTags: [],
                filterFields: ["gender", "brand"],
            });
    });

    it('should handle CLEAR_SNAP', () => {
        expect(PhotoReducer(undefined, {type: CLEAR_SNAP}))
            .toEqual({
                uri: '',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: false,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle GET_TAGS', () => {
        expect(PhotoReducer(undefined, {type: GET_TAGS}))
            .toEqual({
                uri: '',
                data: null,
                loading: true,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: false,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle GET_TAGS_FAIL', () => {
        const error: string = 'An error occurred while fetching data from the server.';
        expect(PhotoReducer(undefined, {type: GET_TAGS_FAIL, payload: error}))
            .toEqual({
                uri: '',
                data: null,
                loading: false,
                connected: true,
                error: error,
                logosAndLabels: [],
                tagsLoaded: false,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle GET_TAGS_SUCCESS', () => {
        expect(PhotoReducer(undefined, {type: GET_TAGS_SUCCESS, payload: {data: null, logosAndLabels: []}}))
            .toEqual({
                uri: '',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: true,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle SET_SNAP', () => {
        expect(PhotoReducer(undefined, {type: SET_SNAP, payload: {uri: 'test', data: null, logosAndLabels: []}}))
            .toEqual({
                uri: 'test',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: true,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle SET_URI', () => {
        expect(PhotoReducer(undefined, {type: SET_URI, payload: 'test'}))
            .toEqual({
                uri: 'test',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: false,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle SET_CONNECTED', () => {
       expect(PhotoReducer(undefined, {type: SET_CONNECTED, payload: false}))
           .toEqual({
               uri: '',
               data: null,
               loading: false,
               connected: false,
               error: '',
               logosAndLabels: [],
               tagsLoaded: false,
               extraTags: [],
               filterFields: ["gender", "brand"]
           })
    });

    it('should handle REMOVE_ITEM', () => {
       expect(PhotoReducer(undefined, {type: REMOVE_ITEM, payload: []}))
           .toEqual({
               uri: '',
               data: null,
               loading: false,
               connected: true,
               error: '',
               logosAndLabels: [],
               tagsLoaded: false,
               extraTags: [],
               filterFields: ["gender", "brand"]
           });
    });

    it('should handle RESET_EXTRA_TAGS', () => {
        expect(PhotoReducer(undefined, {type: RESET_EXTRA_TAGS}))
            .toEqual({
                uri: '',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: false,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle SET_LOGOS_AND_LABELS', () => {
        expect(PhotoReducer(undefined, {type: SET_LOGOS_AND_LABELS, payload: []}))
            .toEqual({
                uri: '',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [],
                tagsLoaded: false,
                extraTags: [],
                filterFields: ["gender", "brand"]
            });
    });

    it('should handle ADD_TAG', () => {
        expect(PhotoReducer(undefined, {type: ADD_TAG, payload: {text: 'test', filter:'label', logosAndLabels: []}}))
            .toEqual({
                uri: '',
                data: null,
                loading: false,
                connected: true,
                error: '',
                logosAndLabels: [new Tag('test', true, 'label', 1.1)],
                tagsLoaded: false,
                extraTags: ['test'],
                filterFields: ["gender", "brand"]
            });
    });
});
import {
    RESET_SETTINGS,
    SET_FIRST_PHOTO,
    SET_FLASH,
    SET_PERMISSION,
    SET_TYPE,
    SET_ZOOM
} from '../actions/types';

const INITIAL_STATE = {
    flash: 0,
    zoom: 0,
    type: 'back',
    permissionsGranted: false,
    firstPhoto: ''
};

export default (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case SET_FLASH:
            return {...state, flash: action.payload};
        case SET_ZOOM:
            return {...state, zoom: action.payload};
        case SET_TYPE:
            return {...state, type: action.payload};
        case SET_PERMISSION:
            return {...state, permissionsGranted: action.payload};
        case SET_FIRST_PHOTO:
            return {...state, firstPhoto: action.payload};
        case RESET_SETTINGS:
            return {...state, flash: 0, zoom: 0, type: 'back'};
        default:
            return state;
    }
};
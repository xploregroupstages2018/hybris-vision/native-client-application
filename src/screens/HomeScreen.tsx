import React, { Component } from 'react';
import { Container, Content, Header, Body, Title, Text } from 'native-base';
import { View, TouchableOpacity, StyleSheet, StatusBar, AsyncStorage } from 'react-native';
import { Ionicons, Octicons, MaterialCommunityIcons } from '@expo/vector-icons';
import {
    HOME_SCREEN_ICON_SIZE,
    HISTORY,
    CAMERA,
    GALLERY,
    DEVICE_HEIGHT,
    getAdjustedFontSize,
    INFO, FIRST_BOOT
} from '../config/Constants';
import { LinearGradient } from 'expo';

class HomeScreen extends Component {
    static navigationOptions = {
        header: null
    };

    componentWillMount() {
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content');
    }

    async onIconPressed(nextScreen: string, backgroundColor: string) {
        const {navigation} = this.props as any;
        let firstBoot = await AsyncStorage.getItem(FIRST_BOOT);
        if (firstBoot !== 'false') {
            navigation.navigate(INFO, {general: 'false', nextScreen, backgroundColor});
        } else {
            if (nextScreen === CAMERA) {
                StatusBar.setHidden(true);
            }
            navigation.navigate(nextScreen);
        }
    }

    renderButtons() {
        const {navigation} = this.props as any;
        const {container, cardStyle, textStyle, cardContentStyle, first, second, third, fourth, shadowStyle,
                redShadow, greenShadow, blueShadow, yellowShadow, buttonStyle, secondShadowStyle, thirdShadowStyle,
                secondRedShadow, secondGreenShadow, secondBlueShadow, secondYellowShadow, thirdBlueShadow,
                thirdGreenShadow, thirdRedShadow, thirdYellowShadow} = styles;
        return (

          <Content style={container}>
              <TouchableOpacity onPress={() => this.onIconPressed(CAMERA, '#fb7686')}>
                  <View style={[cardStyle]}>
                      <View style={[cardContentStyle, first]}>
                          <Text style={textStyle}>TAKE PICTURE</Text>
                          <View style={buttonStyle}>
                              <Ionicons  name="md-camera" size={HOME_SCREEN_ICON_SIZE} color="white"/>
                          </View>
                      </View>
                      <View style={[shadowStyle, redShadow]}/>
                      <View style={[secondShadowStyle, secondRedShadow]}/>
                      <View style={[thirdShadowStyle, thirdRedShadow]}/>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.onIconPressed(GALLERY, '#0ddba7')}>
                  <View style={[cardStyle]}>
                      <View style={[cardContentStyle, second]}>
                          <Text style={textStyle}>CHOOSE FROM GALLERY</Text>
                          <View style={buttonStyle}>
                              <Ionicons  name="md-photos" size={HOME_SCREEN_ICON_SIZE} color="white"/>
                          </View>
                      </View>
                      <View style={[shadowStyle, greenShadow]}/>
                      <View style={[secondShadowStyle, secondGreenShadow]}/>
                      <View style={[thirdShadowStyle, thirdGreenShadow]}/>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate(HISTORY)}>
                  <View style={[cardStyle]}>
                      <View style={[cardContentStyle, third]}>
                          <Text style={textStyle}>HISTORY</Text>
                          <View style={buttonStyle}>
                              <Octicons name="history" size={HOME_SCREEN_ICON_SIZE} color="white"/>
                          </View>
                      </View>
                      <View style={[shadowStyle, blueShadow]}/>
                      <View style={[secondShadowStyle, secondBlueShadow]}/>
                      <View style={[thirdShadowStyle, thirdBlueShadow]}/>
                  </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate(INFO, {general: 'true'})}>
                  <View style={[cardStyle]}>
                      <View style={[cardContentStyle, fourth]}>
                          <Text style={textStyle}>INFO</Text>
                          <View style={buttonStyle}>
                              <MaterialCommunityIcons  name="information-outline" size={HOME_SCREEN_ICON_SIZE} color="white"/>
                          </View>
                      </View>
                      <View style={[shadowStyle, yellowShadow]}/>
                      <View style={[secondShadowStyle, secondYellowShadow]}/>
                      <View style={[thirdShadowStyle, thirdYellowShadow]}/>
                  </View>
              </TouchableOpacity>
          </Content>

        );
    }

    render() {
        const {headerStyle} = styles;
        return (
            <Container>
                <Header style={headerStyle} androidStatusBarColor="#273c75" iosBarStyle="light-content">
                    <Body>
                    <Title style={{color: 'white'}}>HOME</Title>
                    </Body>
                </Header>
                <LinearGradient colors={['#273c75', '#aebbc7']} style={{flex: 1}}>
                {this.renderButtons()}
                </LinearGradient>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerStyle: { backgroundColor: '#273c75', elevation: 0 },
    container: {
        flex: 1
    },
    cardStyle: {
        height: DEVICE_HEIGHT / 4 - 30,
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 15,
        marginRight: 15,
        borderRadius: 8,
        flexDirection: 'row',
    },
    cardContentStyle: {
        marginLeft: 0,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8,
    },
    shadowStyle: {
        width: 8,
    },
    secondShadowStyle: {
        width: 8,
    },
    thirdShadowStyle: {
        width: 8,
        borderTopRightRadius: 8,
        borderBottomRightRadius: 8,
        zIndex: 1,
    },
    textStyle: {
        fontSize: getAdjustedFontSize(26),
        color: 'white',
        fontWeight: '800',
        width: 200,
        marginLeft: 15
    },
    buttonStyle: {
        marginRight: 15
    },
    first: {
        backgroundColor: '#fb7686',
    },
    second: {
        backgroundColor: '#0ddba7',
    },
    third: {
        backgroundColor: '#1ed1e0',
    },
    fourth: {
        backgroundColor: '#f7c738'
    },
    redShadow: {
        backgroundColor: 'rgba(251,118,134, 0.7)'
    },
    greenShadow: {
        backgroundColor: 'rgba(13,219,167, 0.7)'
    },
    blueShadow: {
        backgroundColor: 'rgba(30,209,224, 0.7)'
    },
    yellowShadow: {
        backgroundColor: 'rgba(247, 199, 56, 0.7)'
    },
    secondRedShadow: {
        backgroundColor: 'rgba(251,118,134, 0.4)'
    },
    secondGreenShadow: {
        backgroundColor: 'rgba(13,219,167, 0.4)'
    },
    secondBlueShadow: {
        backgroundColor: 'rgba(30,209,224, 0.4)'
    },
    secondYellowShadow: {
        backgroundColor: 'rgba(247, 199, 56, 0.4)'
    },
    thirdRedShadow: {
        backgroundColor: 'rgba(251,118,134, 0.2)'
    },
    thirdGreenShadow: {
        backgroundColor: 'rgba(13,219,167, 0.2)'
    },
    thirdBlueShadow: {
        backgroundColor: 'rgba(30,209,224, 0.2)'
    },
    thirdYellowShadow: {
        backgroundColor: 'rgba(247, 199, 56, 0.2)'
    }
});

export default (HomeScreen);

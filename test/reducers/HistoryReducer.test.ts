import HistoryReducer from '../../src/reducers/HistoryReducer';
import {ADD_IMAGES_TO_HISTORY, CLEAR_IMAGES} from '../../src/actions/types';

describe('History reducer', () => {
    it('should return the initial state', () => {
        expect(HistoryReducer(undefined, {}))
            .toEqual({images: []});
    });

    it('should handle ADD_IMAGES_TO_HISTORY', () => {
        expect(HistoryReducer(undefined, {type: ADD_IMAGES_TO_HISTORY, payload: []}))
            .toEqual({images: []})
    });

    it('should handle CLEAR_IMAGES', () => {
        expect(HistoryReducer(undefined, {type: CLEAR_IMAGES}))
            .toEqual({images: []})
    });
});
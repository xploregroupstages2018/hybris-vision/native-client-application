import { ADD_IMAGES_TO_HISTORY, CLEAR_IMAGES } from '../actions/types';

const INITIAL_STATE = {
    images: []
};

export default (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case ADD_IMAGES_TO_HISTORY:
            return {...state, images: action.payload};
        case CLEAR_IMAGES:
            return {...state, images: []};
        default:
            return state;
    }
};
import { ADD_IMAGES_TO_HISTORY, CLEAR_IMAGES } from './types';

export const addImagesToHistory = (images: Array<Object>) => {
    return {type: ADD_IMAGES_TO_HISTORY, payload: images};
};

export const clearImages = () => {
    return {type: CLEAR_IMAGES};
};
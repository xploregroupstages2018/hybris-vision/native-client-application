import expect from 'expect';
import Snap from "../../src/model/Snap";
import LocalStorageHelper from "../../src/helpers/LocalStorageHelper";
import {AsyncStorage} from "react-native";
import {IMAGE_MAP} from "../../src/config/Constants";


describe('local storage helper', () => {

    beforeAll(() => {
        AsyncStorage.setItem(IMAGE_MAP, JSON.stringify([new Snap('test', {})]));
    });

    it('should check if snap already exist', () => {
        let snap: Snap = new Snap('', {});
        let snapArray: Array<Snap> = [new Snap('', {})];
        expect(LocalStorageHelper.checkIfUriExists(snapArray, snap)).toEqual(0);
    });

    it('should add aa snap', () => {
        let snap: Snap = new Snap('', {});
        let snapArray: Array<Snap> = [];
        expect(LocalStorageHelper.addSnap(snapArray, snap)).toEqual([snap]);
    });

    it('should save a snap to the local storage', () => {
        expect(LocalStorageHelper.saveSnapToLocalStorage(new Snap('', {})))
            .toEqual({"_40": 0, "_55": null, "_65": 0, "_72": null});
    });

    it('should add snap to empty array', () => {
        let snap: Snap = new Snap('', {});
        expect(LocalStorageHelper.addSnap([], snap)).toEqual([snap]);
    });

    it('should perform a shift operation', () => {
        let snapArray = [
            new Snap('uri1', {}), new Snap('uri2', {}), new Snap('uri3', {}), new Snap('uri4', {}),
            new Snap('uri5', {}), new Snap('uri6', {}), new Snap('uri7', {}), new Snap('uri8', {}),
            new Snap('uri9', {}), new Snap('uri10', {})
        ];
        const snap = new Snap('uri11', {});
        let newSnapArray = [
            new Snap('uri2', {}), new Snap('uri3', {}), new Snap('uri4', {}),
            new Snap('uri5', {}), new Snap('uri6', {}), new Snap('uri7', {}), new Snap('uri8', {}),
            new Snap('uri9', {}), new Snap('uri10', {}), snap
        ];
        expect(LocalStorageHelper.addSnap(snapArray, snap)).toEqual(newSnapArray);
    });

    it('should replace a snap in local storage', async () => {
        let snap = new Snap('uri1', {});
        let snapArray = [snap];
        await AsyncStorage.clear();
        await AsyncStorage.setItem(IMAGE_MAP, JSON.stringify(snapArray));
    });
});

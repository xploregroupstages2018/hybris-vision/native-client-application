import * as actions from '../../src/actions/GalleryActions';
import {LOAD_GALLERY_PHOTOS, SET_PERMISSION_GALLERY} from '../../src/actions/types';

describe('actions', () => {
    it('should create an action to load gallery photos', () => {
       const images: Array<Object> = [];
       const expectedAction = {
           type: LOAD_GALLERY_PHOTOS,
           payload: images
       };
       expect(actions.loadGalleryPhotos(images)).toEqual(expectedAction);
    });

    it('should create an action to load gallery photos', () => {
        const value: boolean = true;
        const expectedAction = {
            type: SET_PERMISSION_GALLERY,
            payload: value
        };
        expect(actions.setPermissionGallery(value)).toEqual(expectedAction);
    });
});
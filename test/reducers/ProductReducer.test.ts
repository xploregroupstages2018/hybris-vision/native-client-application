import ProductReducer from '../../src/reducers/ProductReducer';
import {
    SET_LOADING_PRODUCTS,
    RESET_PRODUCTS,
    GET_PRODUCTS,
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_FAIL,
    RESET_BRANDS
} from '../../src/actions/types';

describe('Product reducer', () => {
   it('should return the initial state', () => {
      expect(ProductReducer(undefined, {}))
          .toEqual({
              products: [],
              loadingProducts: false,
              productError: '',
              genders: ['Men', 'Women'],
              brands: []
          });
   });

   it('should handle RESET_PRODUCTS', () => {
      expect(ProductReducer(undefined, {type: RESET_PRODUCTS}))
          .toEqual({
              products: [],
              loadingProducts: false,
              productError: '',
              genders: ['Men', 'Women'],
              brands: []
          });
   });

   it('should handle SET_LOADING_PRODUCTS', () => {
      expect(ProductReducer(undefined, {type: SET_LOADING_PRODUCTS, payload: true}))
          .toEqual({
              products: [],
              loadingProducts: true,
              productError: '',
              genders: ['Men', 'Women'],
              brands: []
          });
   });

   it('should handle GET_PRODUCTS', () => {
      expect(ProductReducer(undefined, {type: GET_PRODUCTS}))
          .toEqual({
              products: [],
              loadingProducts: true,
              productError: '',
              genders: ['Men', 'Women'],
              brands: []
          });
   });

   it('should handle GET_PRODUCTS_SUCCESS', () => {
      expect(ProductReducer(undefined, {type: GET_PRODUCTS_SUCCESS, payload: {products: [], brands: []}}))
          .toEqual({
              products: [],
              loadingProducts: false,
              productError: '',
              genders: ['Men', 'Women'],
              brands: []
          });
   });

   it('should handle GET_PRODUCTS_FAIL', () => {
       const error = 'Error while fetching data from zalando';
      expect(ProductReducer(undefined, {type: GET_PRODUCTS_FAIL, payload: error}))
          .toEqual({
              products: [],
              loadingProducts: false,
              productError: error,
              genders: ['Men', 'Women'],
              brands: []
          });
   });

    it('should handle RESET_BRANDS', () => {
        expect(ProductReducer(undefined, {type: RESET_BRANDS}))
            .toEqual({
                products: [],
                loadingProducts: false,
                productError: '',
                genders: ['Men', 'Women'],
                brands: []
            });
    });
});
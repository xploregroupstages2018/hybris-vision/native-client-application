import { combineReducers } from 'redux';
import PhotoReducer from './PhotoReducer';
import HistoryReducer from './HistoryReducer';
import CameraReducer from './CameraReducer';
import GalleryReducer from './GalleryReducer';
import ProductReducer from './ProductReducer';

export default combineReducers({
    photo: PhotoReducer,
    history: HistoryReducer,
    camera: CameraReducer,
    gallery: GalleryReducer,
    product: ProductReducer
});
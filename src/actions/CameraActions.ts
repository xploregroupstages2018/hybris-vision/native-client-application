import {
    RESET_SETTINGS,
    SET_FIRST_PHOTO,
    SET_FLASH,
    SET_PERMISSION,
    SET_TYPE,
    SET_ZOOM
} from './types';

export const setFlash = (value: number) => {
    return {type: SET_FLASH, payload: value};
};

export const setZoom = (value: number) => {
    return {type: SET_ZOOM, payload: value};
};
export const setType = (value: string) => {
    return {type: SET_TYPE, payload: value};
};
export const setPermission = (value: boolean) => {
    return {type: SET_PERMISSION, payload: value};
};
export const setFirstPhoto = (value: string) => {
    return {type: SET_FIRST_PHOTO, payload: value};
};

export const resetSettings = () => {
    return {type: RESET_SETTINGS};
};
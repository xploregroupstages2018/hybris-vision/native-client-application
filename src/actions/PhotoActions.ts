import {
    CLEAR_SNAP,
    GET_TAGS,
    GET_TAGS_SUCCESS,
    GET_TAGS_FAIL,
    SET_CONNECTED,
    SET_SNAP,
    SET_URI,
    REMOVE_ITEM, ADD_TAG, RESET_EXTRA_TAGS, SET_LOGOS_AND_LABELS
} from './types';
import Snap from '../model/Snap';
import LocalStorageHelper from '../helpers/LocalStorageHelper';
import Tag from '../model/Tag';

export const setUri = (uri: string) => {
    return {type: SET_URI, payload: uri};
};

export const getTags = (uri: string) => {
    return (dispatch: any) => {
        dispatch(getTagsStarted());
        let formData = new FormData();
        let imageObject = {uri: uri, name: 'photo.jpg', type: 'image/jpg'} as any;
        formData.append('file', imageObject);
        fetch('http://googlevision.elision.zone/identification/image', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: formData
        }).then((response) => response.json())
            .then((jsonResponse) => dispatch(getTagsSuccess(uri, jsonResponse)))
            .catch(() => dispatch(getTagsFail(uri, 'Error while fetching products from Google Vision')));
    };
};

export const getTagsStarted = () => {
  return {type: GET_TAGS};
};

export const getTagsSuccess = (uri: string, data: Object) => {
    let logosAndLabels = getLogosAndLabels(data);
    let snap = new Snap(uri, data, logosAndLabels);
    LocalStorageHelper.saveSnapToLocalStorage(snap);
    return {
        type: GET_TAGS_SUCCESS,
        payload: {data, logosAndLabels}
    };
};

export const getTagsFail = (uri: string, error: string) => {
    let snap = new Snap(uri, {}, []);
    LocalStorageHelper.saveSnapToLocalStorage(snap);
    return {
        type: GET_TAGS_FAIL,
        payload: error
    };
};

export const setSnap = (uri: string, data: Object) => {
    let logosAndLabels = getLogosAndLabels(data);
    return {type: SET_SNAP, payload: {uri, data, logosAndLabels}};
};

export const clearSnap = () => {
    return {type: CLEAR_SNAP};
};

export const setConnected = (value: boolean) => {
    return {type: SET_CONNECTED, payload: value};
};

export const removeItem = (index: number, tags: Array<Tag>) => {
    let newTags: Array<Tag> = tags;
    if (tags.length > 0) {
        newTags[index].enabled = !newTags[index].enabled;
    }
    return {type: REMOVE_ITEM, payload: newTags};
};

export const addTag = (text: string, data: Object) => {
    let logosAndLabels = getLogosAndLabels(data);
    let filter: string = text.toLowerCase() === 'men' || text.toLowerCase() === 'women' ? 'filter' : 'label';
    return {type: ADD_TAG, payload: {text, filter, logosAndLabels}};
};

export const resetExtraTags = () => {
    return {type: RESET_EXTRA_TAGS};
};

export const setLogosAndLabels = (logosAndLabels: Array<Tag>) => {
    return {type: SET_LOGOS_AND_LABELS, payload: logosAndLabels};
};

export const getLogosAndLabels = (parsedData: any) => {
    let tagArray: Array<Tag> = [];
    if (parsedData !== null) {
        let logos = parsedData.logos;
        let newLogos: Array<Tag> = [];
        if (Object.keys(logos).length > 0) {
            newLogos = logos.map((logo: string) => new Tag(logo, true, 'logo', 1.2));
        }
        let webSearch = Object.entries(parsedData.webSearch);
        let newWebSearch: Array<Tag> = [];
        if (Object.keys(webSearch).length > 0) {
            newWebSearch = webSearch.map((search: any) => new Tag(search[0], true, 'label', parseFloat(search[1])));
        }
        let colors = parsedData.colors.slice(0, 2);
        let newColors: Array<Tag> = [];
        if (Object.keys(colors).length > 0) {
            newColors = colors.map((color: string) => new Tag(color, true, 'color', 1));
        }
        tagArray = newLogos.concat(newWebSearch).concat(newColors);
    }
    return tagArray;
};
import { LOAD_GALLERY_PHOTOS, SET_PERMISSION_GALLERY } from './types';

export const loadGalleryPhotos = (images: Array<Object>) => {
    return {type: LOAD_GALLERY_PHOTOS, payload: images};
};

export const setPermissionGallery = (value: boolean) => {
    return {type: SET_PERMISSION_GALLERY, payload: value};
};

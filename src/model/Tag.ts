export default class Tag {
    text: String;
    enabled: boolean;
    type: String;
    importance: Number;

    constructor(text: String, enabled: boolean, type: String, importance: Number) {
        this.text = text;
        this.enabled = enabled;
        this.type = type;
        this.importance = importance;
    }
}
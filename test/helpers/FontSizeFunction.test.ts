import expect from 'expect';
import {getAdjustedFontSize} from "../../src/config/Constants";

describe('font size function', () => {
   it('should return the right font size', () => {
        expect(Math.floor(getAdjustedFontSize(20, 300))).toEqual(18);
   });
});

import React, { Component } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet,
    StatusBar,
    FlatList, Linking
} from 'react-native';
import {
    Container,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button,
    Title,
    Text
} from 'native-base';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { DEVICE_WIDTH, getAdjustedFontSize, HEADER_ICON_SIZE, SNAP } from '../config/Constants';

class ProductDetailScreen extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props: any){
        super(props);
        StatusBar.setBarStyle('light-content');
    }

    onBackButtonClicked() {
        const {navigation} = this.props as any;
        navigation.navigate(SNAP);
    }

    renderProductInfo(product: any) {
        const {productInfoStyle, brandNameStyle, productNameStyle, imageWrapper, imageStyle, infoWrapperStyle} = styles;
        return (
          <View style={productInfoStyle}>
              <View style={imageWrapper}>
                  <Image style={imageStyle} source={{uri: `https://mosaic01.ztat.net/vgs/media/pdp-zoom/${product.media[0].path}`}}/>
              </View>
              <View style={infoWrapperStyle}>
                  <Text style={brandNameStyle}>{product.brand_name}</Text>
                  <Text style={productNameStyle}>{product.name}</Text>
                  <Text/>
                  <Text>Price: {product.price.original}</Text>
                  <Text/>
                  <Text>Sizes: {product.sizes.join('-')}</Text>
              </View>
          </View>
        );
    }

    renderRelatedProducts(product: any) {
        const {imageStyle, relatedProductStyle, listItemStyle} = styles;
        return (
            <FlatList
                data={product.family_articles.slice(1)}
                horizontal={true}
                initialNumToRender={10}
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => (
                    <TouchableOpacity style={relatedProductStyle} onPress={() => Linking.openURL(`https://www.zalando.co.uk/${item.url_key}.html`)}>
                        <View style={listItemStyle}>
                            <Image source={{uri: `https://mosaic01.ztat.net/vgs/media/pdp-zoom/${item.media[0].path}`}} style={imageStyle}/>
                            <Text style={{marginTop: 10, marginBottom: 10}}>{item.name}</Text>
                            <Text>{item.price.original}</Text>
                        </View>
                    </TouchableOpacity>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        );
    }

    render() {
        const {navigation} = this.props as any;
        const product = navigation.getParam('product', 'noproduct');
        const {headerStyle, containerStyle, textStyle, buttonStyle} = styles;
        return (
          <Container style={containerStyle}>
              <Header style={headerStyle} androidStatusBarColor="#273c75" iosBarStyle="light-content">
                  <Left>
                      <Button transparent={true} onPress={() => this.onBackButtonClicked()}>
                          <Ionicons name="md-arrow-back" size={HEADER_ICON_SIZE} color="white"/>
                      </Button>
                  </Left>
                  <Body>
                  <Title style={{color: 'white'}}>{product.name}</Title>
                  </Body>
                  <Right>
                      <Button transparent={true} onPress={() => Linking.openURL(`https://www.zalando.co.uk/${product.url_key}.html`)}>
                          <MaterialIcons name="add-shopping-cart" size={HEADER_ICON_SIZE} color="white"/>
                      </Button>
                  </Right>
              </Header>
              <Content>
                  {this.renderProductInfo(product)}
                  <Button rounded={true} large={true} style={buttonStyle} onPress={() => Linking.openURL(`https://www.zalando.co.uk/${product.url_key}.html`)}>
                      <Text style={{fontSize: getAdjustedFontSize(20), fontWeight: '800'}}>BUY NOW</Text>
                  </Button>
                  {product.family_articles.length > 1 ? <Text style={textStyle}>RELATED PRODUCTS: </Text> : <Text/>}
                  {this.renderRelatedProducts(product)}
              </Content>
          </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerStyle: { backgroundColor: '#273c75', elevation: 0 },
    containerStyle: {flex: 1, backgroundColor: '#a3cbef', justifyContent: 'center'},
    imageWrapper: {
      width: DEVICE_WIDTH / 2
    },
    infoWrapperStyle: {
        width: DEVICE_WIDTH / 2,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 15
    },
    imageStyle: {
        width: DEVICE_WIDTH / 2.8,
        height: DEVICE_WIDTH / 2.8,
        alignSelf: 'center',
        borderRadius: 10,
        paddingBottom: 20
    },
    productInfoStyle: {
        alignSelf: 'center',
        flexDirection: 'row',
        marginTop: 40,
        marginBottom: 30
    },
    productNameStyle: {
        fontSize: getAdjustedFontSize(16),
        fontWeight: '300'
    },
    brandNameStyle: {
        fontSize: getAdjustedFontSize(20),
        fontWeight: '800',
    },
    relatedProductStyle: {
        backgroundColor: '#fff',
        marginRight: 10,
        marginLeft: 10,
        width: DEVICE_WIDTH / 2 - 20,
        borderRadius: 10
    },
    listItemStyle: {
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 20,
        paddingBottom: 20
    },
    textStyle: {
        fontSize: getAdjustedFontSize(22),
        fontWeight: '800',
        marginLeft: 30,
        marginBottom: 15
    },
    buttonStyle: {
        backgroundColor: '#273c75',
        alignSelf: 'center',
        marginBottom: 15
    }
});

export default ProductDetailScreen;
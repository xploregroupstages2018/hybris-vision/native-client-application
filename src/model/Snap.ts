import Tag from './Tag';

export default class Snap {
    uri: string;
    response: Object;
    logosAndLabels: Array<Tag>;

    constructor(uri: string, response: Object, logosAndLabels: Array<Tag>) {
        this.uri = uri;
        this.response = response;
        this.logosAndLabels = logosAndLabels;
    }
}
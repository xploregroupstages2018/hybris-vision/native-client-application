import React, { Component } from 'react';
import {
    View,
    NetInfo,
    Image,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
    StatusBar,
    FlatList,
} from 'react-native';
import {
    Container,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button,
    Title,
    Text,
    Spinner,
} from 'native-base';
import { Ionicons, FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { DEVICE_WIDTH, FILTER, getAdjustedFontSize, HEADER_ICON_SIZE, HOME, PRODUCT } from '../config/Constants';
import { clearSnap, getTags, setConnected, removeItem, resetExtraTags } from '../actions/PhotoActions';
import { getProducts, setLoadingProducts, resetProducts, resetBrands } from '../actions/ProductActions';
import Tag from '../model/Tag';

class ProductScreen extends Component<any, any> {
    static navigationOptions = {
        header: null
    };

    constructor(props: any) {
        super(props);
        StatusBar.setBarStyle('light-content');
    }

    componentDidMount() {
        const {data, uri, setConnected, connected} = this.props;
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
        NetInfo.isConnected.fetch().then(
            (isConnected: boolean) => {
                setConnected(isConnected);
            }
        );
        if (data === null && connected) {
            this.checkInternetConnection(uri);
        }
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
    }

    handleConnectionChange = (isConnected: boolean) => {
        const {setConnected} = this.props;
        setConnected(isConnected);
    }

    checkInternetConnection(uri: string) {
        const {getTags} = this.props;
        getTags(uri);
    }

    renderHeader() {
        const {loading} = this.props;
        if (loading) {
            return (<Spinner color="red"/>);
        } else {
            return (
                <View>
                    <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center'}}>
                        {this.renderTags()}
                    </View>
                </View>
            );
        }
    }

    renderBody() {
        const {loading, connected, error, tagsLoaded} = this.props;
        if (loading || tagsLoaded) {
            return this.renderImage();
        } else if (!connected) {
            return this.renderError('No internet connection.');
        } else if (error !== '') {
            return this.renderError(error);
        } else {
            return (<View/>);
        }
    }

    renderFooter() {
        const {tagsLoaded, products, loadingProducts, logosAndLabels} = this.props;
        if (tagsLoaded && logosAndLabels.length > 0) {
            if (loadingProducts) {
                return (
                    <View>
                        <Spinner color="red"/>
                    </View>
                );
            }
            return this.renderProducts();
        } else {
            return (<View/>);
        }
    }

    render() {
        const {headerStyle, containerStyle} = styles;
        return (
            <Container style={containerStyle}>
                <Header style={headerStyle} androidStatusBarColor="#273c75" iosBarStyle="light-content">
                    <Left>
                        <Button transparent={true} onPress={() => this.onBackButtonClicked()}>
                            <Ionicons name="md-arrow-back" size={HEADER_ICON_SIZE} color="white"/>
                        </Button>
                    </Left>
                    <Body>
                    {this.props.loading ? <Title><Text style={{color: '#fff'}}>SEARCHING...</Text></Title> : <Title/>}
                    </Body>
                    <Right>
                        {this.renderExtraTags()}
                    </Right>
                </Header>
                <Content>
                    {this.renderHeader()}
                    {this.renderBody()}
                    {this.renderFooter()}
                </Content>
            </Container>
        );
    }

    renderImage() {
        const {uri, tagsLoaded, getProducts, loadingProducts, products, logosAndLabels} = this.props;
        const {imageStyle} = styles;
        if (loadingProducts || products.length > 0) {
            return (<View/>);
        } else if (tagsLoaded || (!tagsLoaded && logosAndLabels.length > 0)) {
            return (
                <View>
                    <Image style={imageStyle} source={{uri: uri}}/>
                    <Button rounded={true} primary={true} style={{alignSelf: 'center', backgroundColor: '#273c75'}} onPress={() => getProducts(logosAndLabels)}>
                        <Text style={{fontWeight: '800'}}>SEARCH PRODUCTS</Text>
                    </Button>
                </View>
            );
        } else if (uri !== '') {
            return (
                <View>
                    <Image style={imageStyle} source={{uri: uri}}/>
                </View>
            );
        } else {
            return (<View/>);
        }
    }

    renderError(error: string) {
        const {uri} = this.props;
        const {errorButton, errorTextStyle} = styles;
        return (
            <View>
                <Text style={errorTextStyle}>{error}</Text>
                <Button style={errorButton} rounded={true} onPress={() => this.checkInternetConnection(uri)}>
                    <Text style={{fontWeight: '800'}}>TRY AGAIN</Text>
                </Button>
            </View>
        );
    }

    onBackButtonClicked() {
        const {navigation, clearSnap, setLoadingProducts, resetProducts, resetExtraTags, resetBrands} = this.props;
        navigation.navigate(HOME);
        clearSnap();
        setLoadingProducts(false);
        resetExtraTags();
        resetProducts();
        resetBrands();
    }

    onTagPressed(index: number, tags: Array<Tag>) {
        const {removeItem, setLoadingProducts, resetProducts} = this.props;
        removeItem(index, tags);
        setLoadingProducts(false);
        resetProducts();
    }

    renderTags() {
        const {logosAndLabels, data} = this.props;
        const {tagWrapperStyle, tagStyle, textStyle} = styles;
        if (data !== null && data !== undefined && logosAndLabels.length > 0) {
            if (data === undefined) {
                return (
                    <View>
                        <Text style={textStyle}>No tags retrieved</Text>
                    </View>
                );
            } else {
                return logosAndLabels.map((tag: Tag, index: number) => {
                    let backgroundColor = tag.enabled ? 'rgb(39,60,117)' : 'rgba(39,60,117, 0.4)';
                    return (
                        <TouchableOpacity key={index} style={tagWrapperStyle} onPress={() => this.onTagPressed(index, logosAndLabels)}>
                            <View style={[tagStyle, {backgroundColor: backgroundColor}]}>
                                <Text style={[textStyle, {fontSize: getAdjustedFontSize(20) * (tag.importance as any)}]}>{tag.text}</Text>
                                <TouchableWithoutFeedback>
                                    <FontAwesome name="remove" size={20 * (tag.importance as any)} color="white"/>
                                </TouchableWithoutFeedback>
                            </View>
                        </TouchableOpacity>
                    );
                });
            }
        }
    }

    renderExtraTags() {
        const {tagStyle, textStyle, tagWrapperStyle} = styles;
        const {filterFields, navigation, tagsLoaded, logosAndLabels, brands} = this.props;
        if (tagsLoaded && logosAndLabels.length > 0) {
            return filterFields.map((text: string, index: number) => {
                if (text !== 'brand' || (text === 'brand' && brands.length > 0)) {
                    return (
                        <TouchableOpacity key={index} style={tagWrapperStyle} onPress={() => navigation.navigate(FILTER, {filter: text})}>
                            <View style={[tagStyle, {backgroundColor: '#fff', paddingLeft: 10}]}>
                                <TouchableWithoutFeedback>
                                    <MaterialIcons name="add" size={20} color="#273c75"/>
                                </TouchableWithoutFeedback>
                                <Text style={[textStyle, {color: '#273c75'}]}>{text.toUpperCase()}</Text>
                            </View>
                        </TouchableOpacity>
                    );
                } else {
                    return (<View key={index}/>);
                }
            });
        } else {
            return (<View/>);
        }

    }

    renderProducts() {
        const {products, navigation, tagsLoaded} = this.props;
        const {thumbnailStyle, listItemStyle, productNameStyle, brandNameStyle, productStyle} = styles;
        return (
            <FlatList
                data={products.slice(0, 30)}
                horizontal={false}
                numColumns={2}
                renderItem={({item}) => (
                    <TouchableOpacity style={productStyle} onPress={() => navigation.navigate(PRODUCT, {product: item})}>
                        <View style={listItemStyle}>
                            <Image
                                style={thumbnailStyle}
                                source={{uri: `https://mosaic01.ztat.net/vgs/media/pdp-zoom//${item.media[0].path}`}}
                            />
                            <Text style={brandNameStyle}>{item.brand_name}</Text>
                            <Text style={productNameStyle}>{item.name}</Text>
                        </View>
                    </TouchableOpacity>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        );
    }
}

const styles = StyleSheet.create({
    headerStyle: {backgroundColor: '#273c75', elevation: 0},
    containerStyle: {flex: 1, backgroundColor: '#a3cbef'},
    imageStyle: {
        width: DEVICE_WIDTH / 2,
        height: DEVICE_WIDTH / 2,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 10,
        marginBottom: 25
    },
    tagStyle: {
        marginTop: 3,
        borderRadius: 20,
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        paddingRight: 10
    },
    logoStyle: {
        backgroundColor: '#e55039',
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 20,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tagWrapperStyle: {
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    textStyle: {
        fontSize: getAdjustedFontSize(18),
        fontWeight: '800',
        paddingLeft: 10,
        paddingRight: 15,
        paddingTop: 7.5,
        paddingBottom: 7.5,
        color: '#fff'
    },
    errorTextStyle: {
        fontSize: 22,
        fontWeight: '800',
        alignSelf: 'center',
        marginBottom: 30,
        marginTop: 50,
        marginRight: 50,
        marginLeft: 50
    },
    errorButton: {
        alignSelf: 'center',
        backgroundColor: '#273c75'
    },
    thumbnailStyle: {
        marginBottom: 30,
        width: DEVICE_WIDTH / 2 - 60,
        height: DEVICE_WIDTH / 2 - 60,
        borderRadius: 10
    },
    listItemStyle: {
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 25,
        paddingBottom: 25
    },
    productNameStyle: {
        fontSize: 18,
        fontWeight: '300',
        color: '#000'
    },
    brandNameStyle: {
        fontSize: 22,
        fontWeight: '800',
        color: '#000'
    },
    productStyle: {
        width: DEVICE_WIDTH / 2 - 20,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 10
    }
});

const mapStateToProps = ({photo, product}: { photo: any, product: any }) => {
    const {uri, data, loading, connected, error, logosAndLabels, tagsLoaded, filterFields} = photo;
    const {products, loadingProducts, brands} = product;
    return {uri, data, loading, connected, error, logosAndLabels, products, tagsLoaded, loadingProducts, filterFields, brands};
};

export default connect(mapStateToProps, {
    clearSnap,
    getTags,
    setConnected,
    removeItem,
    getProducts,
    setLoadingProducts,
    resetProducts,
    resetExtraTags,
    resetBrands
})(ProductScreen);
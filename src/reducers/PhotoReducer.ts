import {
    CLEAR_SNAP,
    GET_TAGS,
    GET_TAGS_FAIL,
    GET_TAGS_SUCCESS,
    SET_CONNECTED,
    SET_SNAP,
    SET_URI,
    REMOVE_ITEM, ADD_TAG, RESET_EXTRA_TAGS, SET_LOGOS_AND_LABELS
} from '../actions/types';
import Tag from '../model/Tag';

const INITIAL_STATE = {
    uri: '',
    data: null,
    loading: false,
    connected: true,
    error: '',
    logosAndLabels: [],
    tagsLoaded: false,
    filterFields: ['gender', 'brand'],
    extraTags: []
};

export default (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case SET_URI:
            return {...state, uri: action.payload};
        case GET_TAGS:
            return {...state, loading: true, connected: true, error: '', tagsLoaded: false};
        case GET_TAGS_SUCCESS:
            return {...state, data: action.payload.data, loading: false, error: '',
                    logosAndLabels: action.payload.logosAndLabels, tagsLoaded: true};
        case GET_TAGS_FAIL:
            return {...state, data: null, loading: false, error: action.payload, logosAndLabels: []};
        case SET_SNAP:
            return {...state, data: action.payload.data, uri: action.payload.uri,
                    logosAndLabels: action.payload.logosAndLabels, tagsLoaded: true};
        case CLEAR_SNAP:
            return {...state, uri: '', data: null, loading: false, logosAndLabels: [], error: '', tagsLoaded: false};
        case SET_CONNECTED:
            return {...state, connected: action.payload};
        case REMOVE_ITEM:
            return {...state, logosAndLabels: [].concat(action.payload)};
        case ADD_TAG:
            let extraTags: Array<string> = [...state.extraTags];
            if (!(extraTags.indexOf(action.payload.text) > -1)) {
                extraTags.push(action.payload.text);
            }
            let newExtraTags = extraTags.map((text: string) => new Tag(text, true, action.payload.filter, 1.1));
            return {...state, extraTags: extraTags, logosAndLabels: action.payload.logosAndLabels.concat(newExtraTags)};
        case RESET_EXTRA_TAGS:
            return {...state, extraTags: []};
        case SET_LOGOS_AND_LABELS:
            return {...state, logosAndLabels: action.payload};
        default:
            return state;
    }
};
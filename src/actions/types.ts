export const SET_URI = 'set_uri';
export const GET_TAGS = 'get_tags';
export const GET_TAGS_SUCCESS = 'get_tags_success';
export const GET_TAGS_FAIL = 'get_tags_fail';
export const SET_SNAP = 'set_snap';
export const CLEAR_SNAP = 'clear_snap';
export const SET_CONNECTED = 'set_connected';
export const REMOVE_ITEM = 'remove_item';
export const ADD_TAG = 'add_tag';
export const RESET_EXTRA_TAGS = 'reset_extra_tags';
export const SET_LOGOS_AND_LABELS = 'set_logos_and_labels';

export const ADD_IMAGES_TO_HISTORY = 'add_image_to_history';
export const CLEAR_IMAGES = 'clear_images';

export const SET_FLASH = 'set_flash';
export const SET_ZOOM = 'set_zoom';
export const SET_TYPE = 'set_type';
export const SET_PERMISSION = 'set_permission';
export const SET_FIRST_PHOTO = 'set_first_photo';
export const RESET_SETTINGS = 'reset_settings';

export const LOAD_GALLERY_PHOTOS = 'load_gallery_photos';
export const SET_PERMISSION_GALLERY = 'set_permission_gallery';

export const GET_PRODUCTS = 'get_products';
export const GET_PRODUCTS_SUCCESS = 'get_products_success';
export const GET_PRODUCTS_FAIL = 'get_products_fail';
export const SET_LOADING_PRODUCTS = 'set_loading_products';
export const RESET_PRODUCTS = 'reset_products';
export const RESET_BRANDS = 'reset_brands';
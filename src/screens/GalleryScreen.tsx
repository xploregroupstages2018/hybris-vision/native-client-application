import React, { Component } from 'react';
import { CameraRoll, FlatList, Image, Platform, StatusBar, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { setUri }  from '../actions/PhotoActions';
import { loadGalleryPhotos, setPermissionGallery } from '../actions/GalleryActions';
import { DEVICE_WIDTH, HEADER_ICON_SIZE, HOME, SNAP } from '../config/Constants';
import { Body, Button, Container, Content, Header, Left, Right, Title } from 'native-base';
import { Ionicons } from '@expo/vector-icons';
import { clearSnap } from '../actions/PhotoActions';
import { Permissions } from 'expo';

class GalleryScreen extends Component<any, any> {
    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        const {loadGalleryPhotos} = this.props;
        let config = this.setCameraRollConfig();
        CameraRoll.getPhotos(config as any).then((photos: any) => {
                loadGalleryPhotos(photos.edges);
            }
        );
    }

    componentWillMount() {
        const {setPermissionGallery} = this.props;
        Permissions.askAsync(Permissions.CAMERA_ROLL as any)
            .then((res) => {
                let status = res.status === 'granted';
                setPermissionGallery(status);
            });
    }

    constructor(props: any){
        super(props);
        StatusBar.setBarStyle('light-content');
    }

    setCameraRollConfig() {
        let config = null;
        if (Platform.OS === 'ios') {
            config = {
                first: 100,
                assetType: 'Photos',
                groupTypes: 'Album'
            };
        } else {
            config = {
                first: 100,
                assetType: 'Photos',
            };
        }
        return config;
    }

    selectImage(uri: string) {
        const {setUri, navigation, clearSnap} = this.props;
        clearSnap();
        setUri(uri);
        navigation.navigate(SNAP);
    }

    render() {
        const {containerStyle, headerStyle} = styles;
        const {navigation} = this.props;
        return (
            <Container style={containerStyle}>
                <Header style={headerStyle} androidStatusBarColor="#273c75" iosBarStyle="light-content">
                    <Left>
                        <Button transparent={true} onPress={() => navigation.navigate(HOME)}>
                            <Ionicons name="md-arrow-back" size={HEADER_ICON_SIZE} color="white"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={{color: 'white'}}>GALLERY</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    {this.renderGallery()}
                </Content>
            </Container>
        );
    }

    renderGallery() {
        const {imageStyle} = styles;
        const {photos} = this.props;
        return (
            <FlatList
                data={photos}
                horizontal={false}
                numColumns={3}
                initialNumToRender={30}
                renderItem={({item}) => (
                    <TouchableWithoutFeedback onPress={() => this.selectImage(item.node.image.uri)}>
                        <Image
                            style={imageStyle}
                            source={{uri: item.node.image.uri}}
                        />
                    </TouchableWithoutFeedback>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        );
    }
}

const styles = StyleSheet.create({
    headerStyle: { backgroundColor: '#273c75', elevation: 0 },
    containerStyle: {
        flex: 1,
        backgroundColor: 'black',
        marginTop: -1
    },
    imageStyle: {
        width: DEVICE_WIDTH / 3,
        height: DEVICE_WIDTH / 3,
        borderWidth: 0.2,
        borderColor: 'white'
    },
});

const mapStateToProps = ({gallery}: { gallery: any }) => {
    const {photos} = gallery;
    return {photos};
};

export default connect(mapStateToProps, {setUri, loadGalleryPhotos, clearSnap, setPermissionGallery})(GalleryScreen);
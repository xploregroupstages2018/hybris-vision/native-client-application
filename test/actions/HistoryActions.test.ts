import * as actions from '../../src/actions/HistoryActions';
import {ADD_IMAGES_TO_HISTORY, CLEAR_IMAGES} from '../../src/actions/types';
import Snap from '../../src/model/Snap';

describe('actions', () => {
   it('should create an action to add an image to history', () => {
       const snaps: Array<Snap> = [];
       const expectedAction = {
           type: ADD_IMAGES_TO_HISTORY,
           payload: snaps
       };
       expect(actions.addImagesToHistory(snaps)).toEqual(expectedAction);
   }) ;

    it('should create an action to clear history', () => {
        const expectedAction = {
            type: CLEAR_IMAGES
        };
        expect(actions.clearImages()).toEqual(expectedAction);
    }) ;
});

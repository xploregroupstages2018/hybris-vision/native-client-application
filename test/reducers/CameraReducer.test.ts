import CameraReducer from '../../src/reducers/CameraReducer';
import { RESET_SETTINGS, SET_FIRST_PHOTO, SET_FLASH, SET_PERMISSION, SET_TYPE, SET_ZOOM } from '../../src/actions/types';

describe('Camera reducer', () => {
    it('should return the initial state', () => {
        expect(CameraReducer(undefined, {}))
            .toEqual({
                flash: 0,
                zoom: 0,
                type: 'back',
                permissionsGranted: false,
                firstPhoto: ''
            });
    });

    it('should handle RESET_SETTINGS', () => {
        expect(CameraReducer(undefined, {type: RESET_SETTINGS}))
            .toEqual({
                flash: 0,
                zoom: 0,
                type: 'back',
                permissionsGranted: false,
                firstPhoto: ''
            });
    });

    it('should handle SET_FIRST_PHOTO', () => {
        expect(CameraReducer(undefined, {type: SET_FIRST_PHOTO, payload: 'test'}))
            .toEqual({
                flash: 0,
                zoom: 0,
                type: 'back',
                permissionsGranted: false,
                firstPhoto: 'test'
            });
    });

    it('should handle SET_FLASH', () => {
        expect(CameraReducer(undefined, {type: SET_FLASH, payload: 2}))
            .toEqual({
                flash: 2,
                zoom: 0,
                type: 'back',
                permissionsGranted: false,
                firstPhoto: ''
            });
    });

    it('should handle SET_PERMISSION', () => {
        expect(CameraReducer(undefined, {type: SET_PERMISSION, payload: true}))
            .toEqual({
                flash: 0,
                zoom: 0,
                type: 'back',
                permissionsGranted: true,
                firstPhoto: ''
            });
    });

    it('should handle SET_TYPE', () => {
        expect(CameraReducer(undefined, {type: SET_TYPE, payload: 'front'}))
            .toEqual({
                flash: 0,
                zoom: 0,
                type: 'front',
                permissionsGranted: false,
                firstPhoto: ''
            });
    });

    it('should handle SET_ZOOM', () => {
        expect(CameraReducer(undefined, {type: SET_ZOOM, payload: 0.5}))
            .toEqual({
                flash: 0,
                zoom: 0.5,
                type: 'back',
                permissionsGranted: false,
                firstPhoto: ''
            });
    });
});
import { Dimensions } from 'react-native';

export const HOME_SCREEN_ICON_SIZE = 60;
export const HEADER_ICON_SIZE = 30;
export const CAMERA_ICONS_SIZE = 40;
export const HOME = 'home';
export const SNAP = 'snap';
export const HISTORY = 'history';
export const CAMERA = 'camera';
export const IMAGE_MAP = 'image_map';
export const GALLERY = 'gallery';
export const PRODUCT = 'product';
export const INFO = 'info';
export const FILTER = 'filter';
export const DEVICE_WIDTH = Dimensions.get('window').width;
export const DEVICE_HEIGHT = Dimensions.get('window').height;
export const HISTORY_SIZE = 10;
export const FIRST_BOOT = 'first_boot';

export const getAdjustedFontSize = (size: number, width = DEVICE_WIDTH) => {
  return size * width * (1.8 - 0.002 * width) / 400;
};
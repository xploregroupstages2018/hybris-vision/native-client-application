import React, { Component } from 'react';
import {
    Body,
    Button,
    Container,
    Content,
    Header,
    Left,
    List,
    ListItem,
    Right,
    Text,
    Title,
    Thumbnail
} from 'native-base';
import { AsyncStorage, TouchableOpacity, StyleSheet, StatusBar } from 'react-native';
import { DEVICE_HEIGHT, getAdjustedFontSize, HEADER_ICON_SIZE, IMAGE_MAP, SNAP } from '../config/Constants';
import { connect } from 'react-redux';
import { addImagesToHistory, clearImages } from '../actions/HistoryActions';
import { Entypo, Ionicons } from '@expo/vector-icons';
import { setSnap } from '../actions/PhotoActions';
import { setLoadingProducts, resetProducts } from '../actions/ProductActions';
import Snap from '../model/Snap';
import Tag from '../model/Tag';

class HistoryScreen extends Component<any, any> {
    static navigationOptions = {
        header: null
    };

    constructor(props: any){
        super(props);
        StatusBar.setBarStyle('light-content');
    }

    componentDidMount() {
        const {addImagesToHistory} = this.props;
        const images = AsyncStorage.getItem(IMAGE_MAP);
        images.then((res) => {
            if (res != null) {
                let parsedResponse = JSON.parse(res);
                addImagesToHistory(parsedResponse);
            }
        });
    }

    removeImageFromLocalStorage(rowID: number) {
        const {addImagesToHistory} = this.props;
        AsyncStorage.getItem(IMAGE_MAP)
            .then((res) => {
                let newImageArray = JSON.parse(res);
                newImageArray.splice(rowID, 1);
                AsyncStorage.setItem(IMAGE_MAP, JSON.stringify(newImageArray));
                addImagesToHistory(newImageArray);
            });
    }

    clearLocalStorage() {
        const {clearImages} = this.props;
        AsyncStorage.getItem(IMAGE_MAP)
            .then((res) => {
                let imageArray = JSON.parse(res);
                if (imageArray != null) {
                    AsyncStorage.clear();
                    clearImages();
                }
            });
    }

    onListItemClicked(snap: Snap) {
        const {setSnap, navigation} = this.props;
        Object.keys(snap.response).length === 0 ? setSnap(snap.uri, null) : setSnap(snap.uri, snap.response);
        navigation.navigate(SNAP);
    }

    createTagList(logosAndLabels: Array<Tag>) {
        const {listTextStyle} = styles;
/*        if (Object.keys(imageData).length === 0) {
        } else {
        }*/
        if (logosAndLabels.length === 0) {
            return <Text style={listTextStyle}>No tags</Text>;
        } else {
            let labels = logosAndLabels.map((tag: Tag) => {
                return tag.text;
            });
            return <Text style={listTextStyle}>{labels.join(', ')}</Text>;

        }
    }

    renderImages() {
        const {textStyle} = styles;
        const {images} = this.props;
        if (images.length > 0) {
            return (
                <List
                    dataArray={images}
                    renderRow={(rowData, sectionID, rowID) =>
                          <ListItem avatar={true} onPress={() => this.onListItemClicked(rowData)} style={{paddingTop: 10, paddingBottom: 10}}>
                              <Left>
                                  <Thumbnail source={{uri: rowData.uri}}/>
                              </Left>
                              <Body>
                              {this.createTagList(rowData.logosAndLabels)}
                              </Body>
                              <Right>
                                  <TouchableOpacity onPress={() => this.removeImageFromLocalStorage(Number(rowID))}>
                                      <Entypo name="squared-cross" color="red" size={30}/>
                                  </TouchableOpacity>
                              </Right>
                          </ListItem>
                      }
                />
            );
        } else {
            return (
                <Text style={textStyle}>No history yet!</Text>
            );
        }
    }

    render() {
        const {headerStyle, containerStyle, buttonStyle} = styles;
        const {navigation} = this.props;
        return (
            <Container style={containerStyle}>
                <Header style={headerStyle} androidStatusBarColor="#273c75" iosBarStyle="light-content">
                    <Left>
                        <Button transparent={true} onPress={() => navigation.goBack()}>
                            <Ionicons name="md-arrow-back" size={HEADER_ICON_SIZE} color="white"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={{color: 'white'}}>HISTORY</Title>
                    </Body>
                    <Right>
                        <Button onPress={() => this.clearLocalStorage()} style={buttonStyle} rounded={true} small={true}>
                            <Text style={{color: '#273c75'}}>CLEAR</Text>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    {this.renderImages()}
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    headerStyle: { backgroundColor: '#273c75', elevation: 0 },
    containerStyle: {flex: 1, marginTop: -1, backgroundColor: '#a3cbef'},
    textStyle: {
        fontSize: 30,
        alignSelf: 'center',
        marginTop: DEVICE_HEIGHT / 4
    },
    listTextStyle: {
        fontSize: getAdjustedFontSize(18),
        fontWeight: '700'
    },
    buttonStyle: {
        marginTop: 15,
        marginBottom: 15,
        backgroundColor: '#fff'
    }
});

const mapStateToProps = ({history}: { history: any }) => {
    const {images} = history;
    return {images};
};

export default connect(mapStateToProps, {addImagesToHistory, clearImages, setSnap, setLoadingProducts, resetProducts})(HistoryScreen);
import GalleryReducer from '../../src/reducers/GalleryReducer';
import { LOAD_GALLERY_PHOTOS, SET_PERMISSION_GALLERY } from '../../src/actions/types';

describe('Gallery reducer', () => {
   it('should return the initial state', () => {
      expect(GalleryReducer(undefined, {}))
          .toEqual({photos: [], permission: false});
   });

   it('should handle LOAD_GALLERY_PHOTOS', () => {
      expect(GalleryReducer(undefined, {type: LOAD_GALLERY_PHOTOS, payload: []}))
          .toEqual({photos: [], permission: false})
   });

    it('should handle the PERMISSION_GALLERY', () => {
        expect(GalleryReducer(undefined, {type: SET_PERMISSION_GALLERY, payload: true}))
            .toEqual({photos: [], permission: true})
    });
});

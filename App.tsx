import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import HomeScreen from './src/screens/HomeScreen';
import {StackNavigator} from 'react-navigation';
import HistoryScreen from './src/screens/HistoryScreen';
import GalleryScreen from './src/screens/GalleryScreen';
import CameraScreen from './src/screens/CameraScreen';
import ProductScreen from './src/screens/ProductScreen';
import Expo from 'expo';
import {AsyncStorage} from 'react-native';
import {IMAGE_MAP} from './src/config/Constants';
import ProductDetailScreen from './src/screens/ProductDetailScreen';
import InfoScreen from './src/screens/InfoScreen';
import FilterScreen from './src/screens/FilterScreen';

const store = createStore(reducers, applyMiddleware(ReduxThunk));

export default class App extends Component {

    constructor(props: any) {
        super(props);
        this.state = {isReady: false};
    }

    componentWillMount() {
        this.loadFonts().then(() => this.setState({isReady: true}));
/*        AsyncStorage.clear();
        AsyncStorage.getItem(IMAGE_MAP)
            .then((data) => {
                let array = JSON.parse(data);
                if (array != null) {
                    AsyncStorage.clear();
                }
            });*/
    }

    async loadFonts() {
        await Expo.Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
        });
    }

    render() {
        const {isReady} = this.state as any;
        const MainNavigator = StackNavigator({
            home: {screen: HomeScreen},
            camera: {screen: CameraScreen},
            gallery: {screen: GalleryScreen},
            snap: {screen: ProductScreen},
            product: {screen: ProductDetailScreen},
            history: {screen: HistoryScreen},
            info: {screen: InfoScreen},
            filter: {screen: FilterScreen}
        });

        if (!isReady) {
            return <Expo.AppLoading/>;
        } else {
            return (
                <Provider store={store}>
                    <MainNavigator/>
                </Provider>
            );

        }

    }
}

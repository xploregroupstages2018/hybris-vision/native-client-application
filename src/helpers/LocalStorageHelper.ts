import { HISTORY_SIZE, IMAGE_MAP } from '../config/Constants';
import { AsyncStorage } from 'react-native';
import Snap from '../model/Snap';

export default class LocalStorageHelper {
    static saveSnapToLocalStorage = async (result: Snap) => {
        let storage: string = await AsyncStorage.getItem(IMAGE_MAP);
        let parsedStorage = JSON.parse(storage);
        let index = LocalStorageHelper.checkIfUriExists(parsedStorage, result);
        if (index !== -1) {
            parsedStorage[index] = result;
            AsyncStorage.setItem(IMAGE_MAP, JSON.stringify(parsedStorage));
        } else {
            let snaps = LocalStorageHelper.addSnap(parsedStorage, result);
            AsyncStorage.setItem(IMAGE_MAP, JSON.stringify(snaps));
        }
    }

    static checkIfUriExists(parsedStorage: Array<Snap>, result: Snap) {
        let indexToReturn = -1;
        if (parsedStorage !== null) {
            parsedStorage.forEach((snap, index) => {
                if (snap.uri === result.uri) {
                    indexToReturn = index;
                }
            });
        }
        return indexToReturn;
    }

    static addSnap(storage: Array<Snap>, snapToAdd: Snap) {
        if (storage !== null && storage.length > 0) {
            storage.length >= HISTORY_SIZE ? this.shiftArray(storage, snapToAdd) : storage.push(snapToAdd);
            return storage;
        } else {
            return [snapToAdd];
        }
    }

    static shiftArray(snaps: Array<Snap>, snap: Snap) {
        snaps.shift();
        snaps.push(snap);
    }
}
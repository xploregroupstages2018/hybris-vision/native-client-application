import * as actions from '../../src/actions/PhotoActions';
import {
    CLEAR_SNAP,
    GET_TAGS_SUCCESS,
    SET_SNAP,
    SET_URI,
    SET_CONNECTED,
    REMOVE_ITEM,
    ADD_TAG,
    RESET_EXTRA_TAGS,
    SET_LOGOS_AND_LABELS,
    GET_TAGS_FAIL, GET_TAGS
} from '../../src/actions/types';
import expect from 'expect'
import Tag from "../../src/model/Tag";

let data = {
    "colors": [
    ],
    "labels": {
        "footwear": 0.9909343719482422,
        "shoe": 0.9601745009422302,
        "sportswear": 0.9184701442718506,
        "walking shoe": 0.8745435476303101,
    },
    "logos": [
    ],
    "safeSearch": {
        "adult": "VERY_UNLIKELY",
        "medical": "VERY_UNLIKELY",
        "racy": "VERY_UNLIKELY",
        "spoof": "VERY_UNLIKELY",
        "violence": "VERY_UNLIKELY",
    },
    "textList": [],
    "webSearch": {
        "Nike": 0.7091000080108643
    }
};

let tag = new Tag("Nike", true, 'label', 0.7091000080108643);
let tagArray: Array<Tag> = [tag];

describe('actions', () => {
    it('should create an action to clear the value of a snap', () => {
        const expectedAction = {
            type: CLEAR_SNAP
        };
        expect(actions.clearSnap()).toEqual(expectedAction);
    });

    it('should create an action to set the content of a snap', () => {
        const uri: string = '';
        const data: any = null;
        const logosAndLabels: Array<Tag> = [];
        const expectedAction = {
            type: SET_SNAP,
            payload: {uri, data, logosAndLabels}
        };
        expect(actions.setSnap(uri, data)).toEqual(expectedAction);
    });

    it('should create an action to set the uri of a snap', () => {
        const uri: string = '';
        const expectedAction = {
            type: SET_URI,
            payload: uri
        };
        expect(actions.setUri(uri)).toEqual(expectedAction);
    });

    it('should create an action to set the connection value', () => {
        const value: boolean = true;
        const expectedAction = {
            type: SET_CONNECTED,
            payload: value
        };
        expect(actions.setConnected(value)).toEqual(expectedAction);
    });

    it('should create an action to remove an item', () => {
        const index: number = 0;
        const tags: Array<Tag> = [];
        const expectedAction = {
            type: REMOVE_ITEM,
            payload: tags
        };
        expect(actions.removeItem(index, tags)).toEqual(expectedAction);
    });

    it('should create an action to reset the extra tags (gender, brand)', () => {
        const expectedAction = {
            type: RESET_EXTRA_TAGS
        };
        expect(actions.resetExtraTags()).toEqual(expectedAction);
    });

    it('should create an action to set the the value of logosAndLabels', () => {
        const array: Array<Tag> = [];
        const expectedAction = {
            type: SET_LOGOS_AND_LABELS,
            payload: array
        };
        expect(actions.setLogosAndLabels(array)).toEqual(expectedAction);
    });

    it('should create an action to add a tag', () => {
        const logosAndLabels: Array<Tag> = [];
        const text: string = '';
        const data: any = null;
        const expectedAction = {
            type: ADD_TAG,
            payload: {text: text, filter: 'label', logosAndLabels: logosAndLabels}
        };
        expect(actions.addTag(text, data)).toEqual(expectedAction);
    });

    it('should return empty logosAndLabels', () => {
        expect(actions.getLogosAndLabels(null)).toEqual([]);
    });

    it('should return logosAndLabels', () => {
        expect(actions.getLogosAndLabels(data)).toEqual(tagArray);
    });

    it('should handle the start of fetching tags', () => {
        const expectedAction = {type: GET_TAGS};
        expect(actions.getTagsStarted()).toEqual(expectedAction);
    });

    it('should handle the success of fetching tags', () => {
        const expectedAction = {
            type: GET_TAGS_SUCCESS,
            payload: {data: data, logosAndLabels: tagArray}
        };
        expect(actions.getTagsSuccess('', data)).toEqual(expectedAction);
    });

    it('should handle the failure of fetching tags', () => {
        const error: string = 'error';
        const expectedAction = {
            type: GET_TAGS_FAIL,
            payload: error
        };
        expect(actions.getTagsFail('', error)).toEqual(expectedAction);
    });
});




import {
    GET_PRODUCTS,
    GET_PRODUCTS_FAIL,
    GET_PRODUCTS_SUCCESS, RESET_BRANDS,
    RESET_PRODUCTS,
    SET_LOADING_PRODUCTS
} from './types';
import Tag from '../model/Tag';
import { setLogosAndLabels } from './PhotoActions';

export const getProducts = (logosAndLabels: Array<Tag>) => {
    return (dispatch: any) => {
        dispatch(getProductsStarted());
        fetch(`http://googlevision.elision.zone/commerce/product?provider=zalando`, {
           method: 'POST',
           headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json'
           },
            body: JSON.stringify(logosAndLabels as any)
        }).then((response) => response.json())
            .then((json) => dispatch(getProductsSuccess(json, logosAndLabels)))
            .catch(() => dispatch(getProductsFail('Error while fetching products from zalando')));
    };
};

export const getProductsStarted = () => {
  return {type: GET_PRODUCTS};
};

export const getProductsSuccess = (json: any, currentLogosAndLabels: Array<Tag>) => {
    let newLogosAndLabels = getUsedLogosAndLabels(json, currentLogosAndLabels);
    let brands = getBrands(json);
    setLogosAndLabels(newLogosAndLabels);
    return {type: GET_PRODUCTS_SUCCESS, payload: {products: json.products, brands: brands}};
};

export const getProductsFail = (error: string) => {
    return {type: GET_PRODUCTS_FAIL, payload: error};
};

export const getBrands = (json: any) => {
    let brands: Array<String> = [];
    json.products.forEach((product: any) => {
        if (!(brands.indexOf(product.brand_name) > -1)) {
            brands.push(product.brand_name);
        }
    });
    return brands.sort((a: String, b: String) => {
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });
};

export const getUsedLogosAndLabels = (json: any, currentLogosAndLabels: Array<Tag>) => {
    let textTags = json.tags.map((tag: Tag) => tag.text);
    return currentLogosAndLabels.map((tag: Tag) => {
        if (tag.type !== 'color' && tag.type !== 'filter') {
            tag.enabled = textTags.indexOf(tag.text) > -1;
        }
        return new Tag(tag.text, tag.enabled, tag.type, tag.importance);
    });
};

export const setLoadingProducts = (value: boolean) => {
    return {type: SET_LOADING_PRODUCTS, payload: value};
};

export const resetProducts = () => {
    return {type: RESET_PRODUCTS};
};

export const resetBrands = () => {
    return {type: RESET_BRANDS};
};
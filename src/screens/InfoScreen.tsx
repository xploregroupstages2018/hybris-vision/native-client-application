import React, { Component } from 'react';
import {
    Container,
    Content,
    Header,
    Left,
    Body,
    Right,
    Button,
    Title,
    Text
} from 'native-base';
import { AsyncStorage, StatusBar, StyleSheet, View, Image } from 'react-native';
import { DEVICE_WIDTH, FIRST_BOOT, getAdjustedFontSize, HEADER_ICON_SIZE, HOME } from '../config/Constants';
import { Ionicons } from '@expo/vector-icons';

class InfoScreen extends Component<any, any> {
    static navigationOptions = {
        header: null
    };

    constructor(props: any) {
        super(props);
        StatusBar.setBarStyle('light-content');
    }

    render() {
        const {headerStyle, containerStyle, textStyle, imageStyle} = styles;
        const {navigation} = this.props;
        const backgroundColor = navigation.getParam('backgroundColor', '#f7c738');
        return (
            <Container>
                <Header style={headerStyle} androidStatusBarColor="#273c75" iosBarStyle="light-content">
                    <Left>
                        <Button transparent={true} onPress={() => navigation.navigate(HOME)}>
                            <Ionicons name="md-arrow-back" size={HEADER_ICON_SIZE} color="white"/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={{color: 'white'}}>INFO</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content contentContainerStyle={[containerStyle, {backgroundColor: backgroundColor}]}>
                    <Image style={imageStyle} source={require('../assets/shoe.jpg')} />
                    <Text style={textStyle}>-Make sure you have an egalized background.</Text>
                    <Text style={textStyle}>-Make sure there are no other objects in the picture.</Text>
                    <Text style={textStyle}>-Make sure the whole object is clearly visible.</Text>
                    {this.renderButton(backgroundColor)}
                </Content>
            </Container>
        );
    }

    onButtonPressed(nextScreen: string) {
        const {navigation} = this.props;
        AsyncStorage.setItem(FIRST_BOOT, 'false');
        navigation.navigate(nextScreen);
    }

    renderButton(backgroundColor: string) {
        const {buttonStyle} = styles;
        const {navigation} = this.props;
        const general = navigation.getParam('general', '');
        const nextScreen = navigation.getParam('nextScreen', '');
        if (general === 'false') {
            return (
                <Button rounded={true} large={true} style={buttonStyle} onPress={() => this.onButtonPressed(nextScreen)}>
                    <Text style={{color: backgroundColor, fontWeight: '800'}}>OK, I GET IT</Text>
                </Button>
            );
        } else {
            return (<View/>);
        }
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        alignItems: 'center'

    },
    headerStyle: {
        backgroundColor: '#273c75',
        elevation: 0
    },
    textStyle: {
        color: 'white',
        fontSize: getAdjustedFontSize(22),
        fontWeight: '800',
        marginBottom: 20,
    },
    buttonStyle: {
        alignSelf: 'center',
        marginTop: 100,
        backgroundColor: '#fff'
    },
    imageStyle: {
        width: 150,
        height: 150,
        borderRadius: 75,
        alignSelf: 'center',
        marginTop: 50,
        marginBottom: 50
    }
});

export default InfoScreen;
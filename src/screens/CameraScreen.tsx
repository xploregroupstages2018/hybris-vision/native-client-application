import { Camera, Permissions } from 'expo';
import React, { Component } from 'react';
import {
    CameraRoll,
    Image,
    PanResponder,
    PanResponderInstance,
    Platform, StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { CAMERA_ICONS_SIZE, DEVICE_HEIGHT, GALLERY, HOME, SNAP } from '../config/Constants';
import { connect } from 'react-redux';
import { setUri } from '../actions/PhotoActions';
import { setFirstPhoto, setFlash, setPermission, setType, setZoom, resetSettings } from '../actions/CameraActions';
import { Entypo, MaterialIcons, SimpleLineIcons } from '@expo/vector-icons';
import { clearSnap } from '../actions/PhotoActions';

class CameraScreen extends Component<any, any> {
    static navigationOptions = {
        header: null
    };
    private _panResponder: PanResponderInstance;
    private camera: any = Camera;

    constructor(props: any) {
        super(props);
        const touchThreshold = 5;
        this._panResponder = PanResponder.create({
                onStartShouldSetPanResponder: (evt, gestureState) => false,
                onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
                onMoveShouldSetPanResponder: (evt, gestureState) => {
                    const {dx, dy} = gestureState;
                    return (Math.abs(dx) > touchThreshold) || (Math.abs(dy) > touchThreshold);
                },
                onMoveShouldSetPanResponderCapture: (evt, gestureState) => false,
                onPanResponderMove: (evt, gestureState) => {
                    if (gestureState.numberActiveTouches === 2) {
                        this.zoom(gestureState.dx, gestureState.dy);
                    }
                }
            }
        );
    }

    componentWillMount() {
        const {setPermission} = this.props;
        Permissions.askAsync(Permissions.CAMERA)
            .then((res) => {
                let status = res.status === 'granted';
                setPermission(status);
            });
    }

    componentDidMount() {
        this.getFirstGalleryImage();
    }

    toggleFacing() {
        const {type, setType} = this.props;
        if (type === 'back') {
            setType('front');
        } else {
            setType('back');
        }
    }

    toggleFlash() {
        const {flash, setFlash} = this.props;
        if (flash === 0) {
            let value = Platform.OS === 'ios' ? 3 : 2;
            setFlash(value);
        } else {
            setFlash(0);
        }
    }

    zoom(dx: number, dy: number) {
        const {zoom, setZoom} = this.props;
        let config = Platform.OS === 'ios' ? 0.005 : 0.05;
        if (dx <= 0 && dy <= 0) {
            setZoom(zoom + config > 1 ? 1 : zoom + config);
        } else {
            setZoom(zoom - config < 0 ? 0 : zoom - config);
        }
    }

    takePicture = async () => {
        const {navigation, setUri, clearSnap} = this.props;
        if (this.camera) {
            this.camera.takePictureAsync({quality: 1}).then((data: any) => {
                clearSnap();
                setUri(data.uri);
                navigation.navigate(SNAP);
                StatusBar.setHidden(false);
            });
        }
    }

    renderNoPermissions() {
        const {warningStyle, warningTextStyle} = styles;
        return (
            <View style={warningStyle}>
                <Text style={warningTextStyle}>
                    Camera permissions not granted - cannot open camera preview.
                </Text>
            </View>
        );
    }

    getFirstGalleryImage() {
        const {setFirstPhoto} = this.props;
        let config = this.setCameraRollConfig();
        CameraRoll.getPhotos(config as any)
            .then((photo: any) => {
                setFirstPhoto(photo.edges[0].node.image.uri);
            });
    }

    setCameraRollConfig() {
        let config = null;
        if (Platform.OS === 'ios') {
            config = {
                first: 1,
                assetType: 'Photos',
                groupTypes: 'Album'
            };
        } else {
            config = {
                first: 1,
                assetType: 'Photos',
                groupName: 'Camera'
            };
        }
        return config;
    }

    renderFirstImageFromGallery() {
        const {firstPhoto} = this.props;
        const {imageStyle} = styles;
        if (firstPhoto !== '') {
            return (
                <Image source={{uri: firstPhoto}} style={imageStyle}/>
            );
        }
        return <View/>;
    }

    onBackButtonPressed() {
        const {resetSettings, navigation} = this.props;
        resetSettings();
        StatusBar.setHidden(false);
        navigation.navigate(HOME);
    }

    renderTopOptions() {
        const {cameraHeader, backButton} = styles;
        return (
            <View style={cameraHeader}>
                <TouchableOpacity onPress={() => this.onBackButtonPressed()} style={backButton}>
                    <MaterialIcons name="arrow-back" size={CAMERA_ICONS_SIZE} color="white"/>
                </TouchableOpacity>
                {this.renderFlash()}
            </View>
        );
    }

    renderBottomOptions() {
        const {cameraFooter, flipButton, galleryButton} = styles;
        const {navigation} = this.props;
        return (
            <View style={cameraFooter}>
                <TouchableOpacity onPress={() => this.toggleFacing()} style={flipButton}>
                    <SimpleLineIcons name="refresh" size={CAMERA_ICONS_SIZE} color="white"/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.takePicture()}>
                    <Entypo name="camera" size={CAMERA_ICONS_SIZE} color="white"/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate(GALLERY)} style={galleryButton}>
                    {this.renderFirstImageFromGallery()}
                </TouchableOpacity>
            </View>
        );
    }

    renderFlash() {
        const {flashButton} = styles;
        const {flash} = this.props;
        let modus = flash === 0 ? 'flash-off' : 'flash-on';
        return (
            <TouchableOpacity onPress={() => this.toggleFlash()} style={flashButton}>
                <MaterialIcons name={modus} size={40} color="white"/>
            </TouchableOpacity>
        );
    }

    renderCamera() {
        const {cameraStyle} = styles;
        const {flash, zoom, type} = this.props;
        return (
                <Camera {...this._panResponder.panHandlers} ref={(ref: any) => { this.camera = ref; }} type={type} zoom={zoom} flashMode={flash} ratio="16:9" autoFocus="on">
                    {this.renderTopOptions()}
                    {this.renderBottomOptions()}
                </Camera>
        );
    }

    render() {
        const {permissionsGranted} = this.props;
        const {containerStyle} = styles;
        const cameraScreenContent = permissionsGranted
            ? this.renderCamera()
            : this.renderNoPermissions();
        return <View style={containerStyle}>{cameraScreenContent}</View>;
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: '#000',
    },
    navigation: {
        flex: 1,
    },
    gallery: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    galleryButton: {
        marginRight: 60,
    },
    cameraHeader: {
        height: DEVICE_HEIGHT * 0.8,
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 20
    },
    cameraFooter: {
        height: DEVICE_HEIGHT * 0.2,
        paddingBottom: 0,
        backgroundColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    backButton: {
        justifyContent: 'flex-start'
    },
    flashButton: {
        marginRight: 20
    },
    cameraStyle: {
        flex: 1,
    },
    flipButton: {
        marginLeft: 60
    },
    warningTextStyle: {
        color: '#fff',
        fontSize: 24
    },
    warningStyle: {
        flex: 1, alignItems: 'center', justifyContent: 'center', padding: 10
    },
    imageStyle: {
        width: 50, height: 50, borderRadius: 50
    }

});

const mapStateToProps = ({camera}: {camera: any}) => {
    const {zoom, flash, permissionsGranted, type, firstPhoto} = camera;
    return {zoom, flash, permissionsGranted, type, firstPhoto};
};

export default connect(mapStateToProps, {
    setUri,
    setFirstPhoto,
    setFlash,
    setPermission,
    setType,
    setZoom,
    resetSettings,
    clearSnap
})(CameraScreen);
import { LOAD_GALLERY_PHOTOS, SET_PERMISSION_GALLERY } from '../actions/types';

const INITIAL_STATE = {
    photos: [],
    permission: false
};

export default (state = INITIAL_STATE, action: any) => {
    switch (action.type) {
        case LOAD_GALLERY_PHOTOS:
            return {...state, photos: action.payload};
        case SET_PERMISSION_GALLERY:
            return {...state, permission: action.payload};
        default:
            return state;
    }
};